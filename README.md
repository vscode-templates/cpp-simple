# cpp-simple-template

A very simple template for single file C++17 projects. Designed for CTCI-style programming questions.

Uses CMAKE and VSCODE - developing inside a docker container.